import { engine } from "./InitMatter"
export function map() {

    var largeur = 1720;
    var hauteur = 780;

    //element gauche
    let cube1 = Bodies.rectangle(380, 870, 150, 50, {
        render: {
            sprite: {
                texture: 'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"

    });
    World.add(engine.world, [cube1]);


    let cube1a = Bodies.rectangle(100, 770, 150, 50, {
        render: {
            sprite: {
                texture: 'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"

    });
    World.add(engine.world, [cube1a]);


    let cube1b = Bodies.rectangle(75, 350, 100, 90, {
        render: {
            sprite: {
                texture: 'Image/rocher.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"

    });
    World.add(engine.world, [cube1b]);


    let cube1c = Bodies.rectangle(380, 650, 100, 90, {
        render: {
            sprite: {
                texture: 'Image/rocher.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"

    });
    World.add(engine.world, [cube1c]);

    let cube1d = Bodies.rectangle(700, 650, 100, 90, {
        render: {
            sprite: {
                texture: 'Image/rocher.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"

    });
    World.add(engine.world, [cube1d]);


    let cube1e = Bodies.rectangle(135, 500, 30, 150, {
        render: {
            sprite: {
                texture: 'Image/rocherp.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"

    });
    World.add(engine.world, [cube1e]);


    //element central
    let cube2 = Bodies.rectangle(1015, 500, 200, 65, {
        render: {
            sprite: {
                texture: 'Image/pierre.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"

    });
    World.add(engine.world, [cube2]);

    //element droit
    let cube3 = Bodies.rectangle(1650, 870, 150, 50, {
        render: {
            sprite: {
                texture: 'Image/saut.png ',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube3]);


    let cube3a = Bodies.rectangle(1920, 770, 150, 50, {
        render: {
            sprite: {
                texture: 'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube3a]);

    let cube3b = Bodies.rectangle(1925, 350, 100, 90, {
        render: {
            sprite: {
                texture: 'Image/rocher.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube3b]);

    let cube3c = Bodies.rectangle(1340, 650, 100, 90, {
        render: {
            sprite: {
                texture: 'Image/rocher.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube3c]);

    let cube3d = Bodies.rectangle(1650, 650, 100, 90, {
        render: {
            sprite: {
                texture: 'Image/rocher.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube3d]);


    let cube3e = Bodies.rectangle(1900, 500, 30, 150, {
        render: {
            sprite: {
                texture: 'Image/rocherp.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"

    });
    World.add(engine.world, [cube3e]);

    //escalier centrale
    let cube4 = Bodies.rectangle(900, 965, 10, 50, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4]);


    let cube4a = Bodies.rectangle(920, 945, 30, 10, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4a]);


    let cube4b = Bodies.rectangle(940, 925, 10, 50, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4b]);


    let cube4c = Bodies.rectangle(960, 905, 30, 10, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4c]);


    let cube4d = Bodies.rectangle(980, 885, 10, 50, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4d]);


    let cube4e = Bodies.rectangle(1020, 865, 70, 10, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4e]);


    let cube4f = Bodies.rectangle(1060, 885, 10, 50, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4f]);


    let cube4g = Bodies.rectangle(1080, 905, 30, 10, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4g]);


    let cube4h = Bodies.rectangle(1100, 925, 10, 50, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4h]);


    let cube4i = Bodies.rectangle(1120, 945, 30, 10, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4i]);


    let cube4j = Bodies.rectangle(1140, 965, 10, 50, {
        render: {
            fillStyle: 'white',
            sprite: {
                texture: '', //'Image/saut.png',
                xScale: 1,
                yScale: 1

            }
        },
        isStatic: true,
        label: "ground"
    });
    World.add(engine.world, [cube4j]);


    // add bodies
    let group = Body.nextGroup(true);
    //modifie du 3eme argument
    let bridge = Composites.stack(1500, 680, 5, 1, 3, 3, function(x, y) {
        return Bodies.rectangle(x - 20, y, 53, 20, {
            collisionFilter: { group: group },
            chamfer: 5,
            density: 0.005,
            frictionAir: 0.05,
            render: {
                fillStyle: 'white',
                sprite: {
                    texture: 'Image/corde1.png',
                    xScale: 1,
                    yScale: 1

                }

            },
            label: "ground"
        });

    });
    //Structure du Pont a modifier le length
    Composites.chain(bridge, 0.3, 0, -0.3, 0, {
        stiffness: 1,
        length: 5,
        render: {
            visible: false
        }
    });



    World.add(engine.world, [
        bridge,


        Constraint.create({
            pointA: { x: 1390, y: 610 },
            bodyB: bridge.bodies[0],
            pointB: { x: -25, y: 0 },
            length: 2,
            stiffness: 0.9
        }),
        Constraint.create({
            pointA: { x: 1600, y: 610 },
            bodyB: bridge.bodies[bridge.bodies.length - 1],
            pointB: { x: 25, y: 0 },
            length: 2,
            stiffness: 0.9
        })
    ]);

    //2ème corde
    let group2 = Body.nextGroup(true);
    //modifie du 3eme argument
    let bridge2 = Composites.stack(480, 680, 5, 1, 3, 3, function(x, y) {
        return Bodies.rectangle(x - 20, y, 53, 20, {
            collisionFilter: { group: group2 },
            chamfer: 5,
            density: 0.005,
            frictionAir: 0.05,
            render: {
                fillStyle: 'white',
                sprite: {
                    texture: 'Image/corde1.png',
                    xScale: 1,
                    yScale: 1

                }

            },
            label: "ground"
        });

    });

    //Structure du Pont a modifier le length
    Composites.chain(bridge2, 0.3, 0, -0.3, 0, {
        stiffness: 1,
        length: 5,
        render: {
            visible: false
        }
    });


    World.add(engine.world, [
        bridge2,

        Constraint.create({
            pointA: { x: 650, y: 610 },
            bodyB: bridge2.bodies[0],
            pointB: { x: -25, y: 0 },
            length: 2,
            stiffness: 0.9
        }),
        Constraint.create({
            pointA: { x: 430, y: 610 },
            bodyB: bridge2.bodies[bridge2.bodies.length - 1],
            pointB: { x: 25, y: 0 },
            length: 2,
            stiffness: 0.9
        })
    ]);


    var bodyA = Bodies.rectangle(100, 200, 150, 50, {
        isStatic: true,
        render: {
            sprite: {
                texture: 'Image/saut.png',
            }
        },
        label: "ground"
    });

    World.add(engine.world, [bodyA]);

    Events.on(engine, 'beforeUpdate', function(event) {


        //pour changer la vitesse la longeur
        var py = 400 + 170 * Math.sin(engine.timing.timestamp * 0.002);
        Body.setVelocity(bodyA, { x: py - bodyA.position.x, y: 0 });
        Body.setPosition(bodyA, { x: py, y: 320 });



    });



    var bodyD = Bodies.rectangle(100, 200, 150, 50, {
        isStatic: true,
        render: {
            sprite: {
                texture: 'Image/saut.png',
            }
        },
        label: "ground"
    });

    World.add(engine.world, [bodyD]);

    Events.on(engine, 'beforeUpdate', function(event) {


        //pour changer la vitesse la longeur
        var py = 1590 + 170 * Math.sin(engine.timing.timestamp * 0.002);
        Body.setVelocity(bodyD, { x: py - bodyD.position.x, y: 0 });
        Body.setPosition(bodyD, { x: py, y: 320 });



    });



}