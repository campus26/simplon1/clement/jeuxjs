export const dataAnimation = {

  "Girl" : [
  {
         loop: true,
         name: "saut",
         nb: 8,
         path: "Image/animationPerso/girlPower/saut/frame-"
        },

      {
        loop: false,
         name: "right",
         nb: 8,
         path: "Image/animationPerso/girlPower/droite/frame-"
        },
        {
            loop: false,
            name: "left",
            nb: 8,
            path: "Image/animationPerso/girlPower/gauche/frame-"
           },
           {
            loop: true,
            name: "tir",
            nb: 14,
            path: "Image/animationPerso/girlPower/tirL/frame-"
           },
           {
            loop: true,
            name: "reposD",
            nb: 10,
            path: "Image/animationPerso/girlPower/reposDroit/frame-"
           },
           {
            loop: true,
            name: "reposL",
            nb: 10,
            path: "Image/animationPerso/girlPower/reposGauche/frame-"
           }

    ],
    "Girl2" : [
        {
               loop: true,
               name: "saut",
               nb: 8,
               path: "Image/animationPerso/Girl2/saut/frame-"
              },
      
            {
              loop: false,
               name: "right",
               nb: 8,
               path: "Image/animationPerso/Girl2/droite/frame-"
              },
              {
                  loop: false,
                  name: "left",
                  nb: 8,
                  path: "Image/animationPerso/Girl2/gauche/frame-"
                 },
                 {
                  loop: true,
                  name: "tir",
                  nb: 14,
                  path: "Image/animationPerso/Girl2/tirL/frame-"
                 },
                 {
                  loop: true,
                  name: "reposD",
                  nb: 9,
                  path: "Image/animationPerso/Girl2/reposDroit/frame-"
                 },
                 {
                  loop: true,
                  name: "reposL",
                  nb: 9,
                  path: "Image/animationPerso/Girl2/reposGauche/frame-"
                 }
      
          ]
}