import {engine} from "./InitMatter"
export class barre {
    constructor(x,y,color) {
        
     this.x=x;
     this.y=y;
     this.color=color;
     this.poucent=0;
     this.val=0;
     this.valide=false;
     this.val1=0;
  

     this.barre = Bodies.rectangle(this.x, this.y, 2, 10,{
        render: {
         fillStyle:this.color,
         lineWidth:1,
         text:{
            content:0,
            color:"white",
            size:30,
            family:"Papyrus",
        }
      
        },
        isStatic: true,
        label:"ground",
        isSensor: true

      
      });
    
      World.add(engine.world, [this.barre]);
    
    }

  
    SetPourcent(){


    this.val1++;
    if (this.poucent<100) {
    this.poucent=(this.val1*100)/500;

    this.barre.render.text.content=  Math.floor(this.poucent)+"%"; 
    this.val=(this.poucent*250)/100;
     
    if (this.val>(this.barre.bounds.max.x-this.barre.bounds.min.x)) {

  var Xmin = this.barre.vertices[0].x;
  var Xmax= this.barre.vertices[1].x+1;
  var TestVertice = [{x: Xmin, y: 720},{x: Xmax, y: 720},{x: Xmax, y: 745},{x: Xmin, y: 745}];
    Body.setVertices(this.barre,TestVertice);

     }
        }

        if ( Math.floor(this.poucent==100)) {

            return true;
        }

    }


    reset(){
        //reset de tout les valeur + de la barre de progression 
        this.val1=0;
        this.poucent=0;
        this.barre.render.text.content=0;
        var Xmin = this.barre.vertices[0].x;
        var TestVertice = [{x: Xmin, y: 720},{x: Xmin+1, y: 720},{x: Xmin+1, y: 745},{x: Xmin, y: 745}];
        Body.setVertices(this.barre,TestVertice);
     
    }


}
