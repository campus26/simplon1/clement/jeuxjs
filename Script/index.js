import {} from "./canvas";
import { music } from "./sound"


document.querySelector('canvas').style.display = 'none';

let button = document.getElementById("submit");
button.addEventListener("click", onSubmit, false);

function onSubmit(a) {
    a.preventDefault();

    document.querySelector('canvas').style.display = 'block';
    document.querySelector('.premier').style.display = 'none';
    document.querySelector('.deuxieme').style.display = 'block';
    music();
}