export let engine = Engine.create();
var largeur = 2000;
var hauteur = 1000;

export let render = Render.create({
    element: document.body,
    engine: engine,
    options: {
        width: largeur,
        height: hauteur,
        wireframes: false,
        background: 'black'

    }
});


var ground = Bodies.rectangle(largeur / 2, hauteur, largeur, 20, { isStatic: true, label: "ground" });
var wallLeft = Bodies.rectangle(0, hauteur / 2, 20, hauteur, { isStatic: true, label: "ground" });
var wallRight = Bodies.rectangle(largeur, hauteur / 2, 20, largeur, { isStatic: true, label: "ground" });



World.add(engine.world, [ground, wallLeft, wallRight]);



Engine.run(engine);
Render.run(render);