import { engine } from "./InitMatter"
import { dataAnimation } from "./dataAnimation"
import { sautSound } from "./sound"


export class Personnage {
    constructor(x, y, h, l, label, img) {

        //definire ces variable pour conaitre l'etat de notre personnage

        this.onJump = false;
        this.jump = false;
        this.gauche = false;
        this.droite = false;
        this.reculer = false;
        this.onBas = true;
        this.nom = label;
        this.onLoop = true;
        this.onRight = true;
        this.image = img;


        this.vie = 3;


        //pour connaite l'orientation du personnage
        this.status = "";

        this.monAnime = dataAnimation[this.image][1];
        this.Loop;
        //compteur pour changer d'image pour le faire bouger
        this.cpt = 1;


        this.canShoot = true;

        this.Perso = Bodies.rectangle(x, y, h, l, {
            render: {
                sprite: {
                    texture: '',
                    xScale: 0.3,
                    yScale: 0.3,
                    yOffset: 0.05

                }
            },
            label: label,
            inertia: Infinity
        });

        World.add(engine.world, [this.Perso]);
        console.log(this.Perso);



    }


    mouvement() {
        if (this.onJump) {
            if (this.canShoot) {


                if (this.onRight) {
                    this.monAnime = dataAnimation[this.image][4];
                } else {
                    this.monAnime = dataAnimation[this.image][5];
                }

            }
        }

        if (this.jump) {
            if (this.onJump) {
                sautSound();
                this.monAnime = dataAnimation[this.image][0];
                Body.setVelocity(this.Perso, { x: 0, y: -11 });
                this.onJump = false;
            }

        }
        if (this.gauche) {

            this.onRight = false;

            Body.setVelocity(this.Perso, { x: -5, y: this.Perso.velocity.y });
            if (this.onJump) {
                this.monAnime = dataAnimation[this.image][2];

            }

        }

        if (this.droite) {
            Body.setVelocity(this.Perso, { x: 5, y: this.Perso.velocity.y });
            this.onRight = true;

            if (this.onJump) {
                this.monAnime = dataAnimation[this.image][1];


            }
        }


        if (this.reculer) {
            Body.setVelocity(this.Perso, { x: 0, y: 10 });

            if (this.onBas) {
                this.onBas = false
                Body.scale(this.Perso, 1, 0.5);
                Body.setInertia(this.Perso, 'Infinity');
            }
        }


        if (this.onLoop) {
            this.myLoop(1);
        }



    }

    GetX() {
        return this.Perso.position.x
    }

    GetY() {
        return this.Perso.position.y
    }

    //methode qui permet de tempo de 1000 ms a chaque tir
    startDelayShoot() {


        // this.monAnime=dataAnimation["Girl"][3];
        var perso = this;
        this.canShoot = false

        setTimeout(function() {
            perso.canShoot = true;
        }, 500)
    }

    afficheVie() {


        for (let index = 0; index < this.vie; index++) {
            var x = document.createElement("img");
            x.id = "iconVie"
            x.src = "Image/vie.png";
            document.getElementById(this.nom).appendChild(x);

        }


    }

    removeLife() {

        this.vie--;
        document.getElementById(this.nom).children[0].remove();
    }





    myLoop(i) {

        if (this.monAnime.loop) {
            this.onLoop = false;
            this.Loop = setTimeout(() => {
                this.Perso.render.sprite.texture = this.monAnime.path + i + '.gif';
                i++
                if (i < this.monAnime.nb) {
                    this.myLoop(i);
                } else {
                    this.onLoop = true;
                }

            }, 65)

        } else {

            var z = Math.floor(this.Perso.position.x * 50);

            if (z % 2 == 1) {
                if (this.cpt == this.monAnime.nb) {
                    this.cpt = 1;
                } else {
                    this.cpt++;
                }
            }
            this.onLoop = true;
            this.Perso.render.sprite.texture = this.monAnime.path + this.cpt + '.gif';

        }

    }




}